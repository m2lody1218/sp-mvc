package com.mvc.sp;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mvc.sp.dao.CommentBoardDAO;
import com.mvc.sp.service.CommentBoardService;
import com.mvc.sp.vo.CommentBoardVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class DAOTest {
	@Autowired
	private CommentBoardDAO cmDAO;
	@Autowired
	private CommentBoardService cmService;
	private CommentBoardVO pCm;
	
	@Before
	public void before() {
		pCm= new CommentBoardVO();
		//cmDAO.insertCommentBoardDAO(pCm);
		pCm.setCmTitle("타이틀");
		pCm.setCmContent("내용");
		pCm.setMiNum(1);
		pCm.setCmNum(1);
	}
	
	@Test
	public void test() {
		//int cnt = cmDAO.insertCommentBoard(pCm);
		//assertThat(cnt,is(1));
		CommentBoardVO cm = cmDAO.selectCommentBoard(pCm);
		assertThat(cm, is(nullValue()));
	}
	

	@Test
	public void selectTest() {
		CommentBoardVO cm = cmService.selectCommentBoard(pCm);
		assertThat(cm, is(nullValue()));
	}


}
