package com.mvc.sp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mvc.sp.service.CommentBoardService;
import com.mvc.sp.vo.CommentBoardVO;
import com.mvc.sp.vo.PageVO;

@Controller
public class CommentBoardController {
	@Autowired
	private CommentBoardService cmService;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@GetMapping("/cms")
	public @ResponseBody List<CommentBoardVO> selectCommentBoard(CommentBoardVO cm,PageVO pageVO)
	{
		if(pageVO.getStartNum()==0) {
			pageVO.setStartNum(1);
			pageVO.setEndNum(10);
		}
		cm.setPageVO(pageVO);
		return cmService.selectCommentBoardList(cm);
	}
	@GetMapping("/cm")
	public @ResponseBody CommentBoardVO selectCommentOne (@ModelAttribute CommentBoardVO cm) {
		logger.info("cm=>{}",cm);
		return cmService.selectCommentBoard(cm);
	}
	
	@PostMapping("/cm2")
	public @ResponseBody int updateCommentBoard (@ModelAttribute CommentBoardVO cm) {
		logger.info("cm=>{}",cm);
		return cmService.updateCommentBoard(cm);
	}
	
	@PostMapping("/cm3")
	public @ResponseBody int deleteCommentBoard (@ModelAttribute CommentBoardVO cm) {
		logger.info("cm=>{}",cm);
		return cmService.deleteCommentBoard(cm);
	}
	
	@PostMapping("/cm4")
	public @ResponseBody int insertCommentBoard (@ModelAttribute CommentBoardVO cm) {
		cm.setMiNum(1);		
		return cmService.insertCommentBoard(cm);
	}
	
}
