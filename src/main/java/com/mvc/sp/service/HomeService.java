package com.mvc.sp.service;

import java.util.List;

public interface HomeService {

	int test();
	List<String> getList();
}
