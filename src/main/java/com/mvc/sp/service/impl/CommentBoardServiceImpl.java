package com.mvc.sp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.sp.dao.CommentBoardDAO;
import com.mvc.sp.service.CommentBoardService;
import com.mvc.sp.vo.CommentBoardVO;

@Service
public class CommentBoardServiceImpl implements CommentBoardService {
	@Autowired
	private CommentBoardDAO cmDAO;
	@Override
	public int insertCommentBoard(CommentBoardVO cm) {
		return cmDAO.insertCommentBoard(cm);
	}

	@Override
	public int updateCommentBoard(CommentBoardVO cm) {
		return cmDAO.updateCommentBoard(cm);
	}

	@Override
	public int deleteCommentBoard(CommentBoardVO cm) {
		return cmDAO.deleteCommentBoard(cm);
	}

	@Override
	public List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cm) {
		return cmDAO.selectCommentBoardList(cm);
	}

	@Override
	public CommentBoardVO selectCommentBoard(CommentBoardVO cm) {
		return cmDAO.selectCommentBoard(cm);
	}


}
