package com.mvc.sp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.sp.dao.HomeDAO;
@Service
public class HomeServiceImpl implements com.mvc.sp.service.HomeService {

	@Autowired
	private HomeDAO hDAO;
	@Override
	public int test() {
		return 0;
	}

	@Override
	public List<String> getList() {
		
		return hDAO.getList();
	}

}
