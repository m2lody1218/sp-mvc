package com.mvc.sp.vo;

import lombok.Data;

@Data
public class HomeVO {
	private String name;
	private int age;
}
