package com.mvc.sp.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mvc.sp.dao.CmCommentDAO;
import com.mvc.sp.vo.CmCommentVO;
import com.mvc.sp.vo.CommentBoardVO;

public class CmCommentDAOImpl implements CmCommentDAO {

	@Autowired
	private SqlSessionFactory ssf;

	@Override
	public int insertCmComment(CommentBoardVO cmc) {
		try (SqlSession ss = ssf.openSession()) {
			return ss.insert("CmCommentDAO.insertCmCommentDAO", cmc);
		}

	}

	@Override
	public int deleteCmComment(int[] cmcNums) {
		try (SqlSession ss = ssf.openSession()) {
			int cnt = 0;
			for(int cmNum : cmcNums) {
				cnt+=ss.delete("CmCommentDAO.deleteCmCommentDAO", cmcNums);
			}
			
			return cnt ;
		}
	}

	@Override
	public int updateCmComment(CommentBoardVO cmc) {
		try (SqlSession ss = ssf.openSession()) {
			return ss.update("CmCommentDAO.updateCmCommentDAO", cmc);
		}
	}

	
	@Override
	public List<CmCommentVO> selectCmCommentList(CommentBoardVO cmc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmCommentVO selectCmComment(CommentBoardVO cmc) {
		try (SqlSession ss = ssf.openSession()) {
			return ss.selectOne("CmCommentDAO.selectCmCommentDAO", cmc);
		}
	}
}
