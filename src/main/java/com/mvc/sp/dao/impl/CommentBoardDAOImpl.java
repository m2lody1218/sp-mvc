package com.mvc.sp.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mvc.sp.dao.CommentBoardDAO;
import com.mvc.sp.vo.CommentBoardVO;

@Repository
public class CommentBoardDAOImpl implements CommentBoardDAO {

	@Autowired
	private SqlSession ss;

	@Override
	public List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cm) {

		return ss.selectList("CommentBoard.selectCommentBoardList", cm);

	}

	@Override
	public int insertCommentBoard(CommentBoardVO cm) {

		return ss.insert("CommentBoard.insertCommentBoard", cm);

	}

	@Override
	public int updateCommentBoard(CommentBoardVO cm) {
		System.out.println(cm);
		return ss.update("CommentBoard.updateCommentBoard", cm);

	}

	@Override
	public CommentBoardVO selectCommentBoard(CommentBoardVO cm) {

		return ss.selectOne("CommentBoard.selectCommentBoard", cm);

	}

	@Override
	public int deleteCommentBoard(CommentBoardVO cm) {
		System.out.println(cm);
		return ss.delete("CommentBoard.deleteCommentBoard", cm);
	}

}
