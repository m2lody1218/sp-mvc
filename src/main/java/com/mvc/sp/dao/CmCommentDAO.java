package com.mvc.sp.dao;

import java.util.List;

import com.mvc.sp.vo.CmCommentVO;
import com.mvc.sp.vo.CommentBoardVO;

public interface CmCommentDAO {
	int insertCmComment(CommentBoardVO cmc);
	int deleteCmComment(int[] cmcNums);
	int updateCmComment(CommentBoardVO cmc);
	List<CmCommentVO> selectCmCommentList(CommentBoardVO cmc);
	CmCommentVO selectCmComment(CommentBoardVO cmc);
}
