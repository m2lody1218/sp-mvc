<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
</head>
<body>

<table>
	
	<tr>
		<th>제목</th>
		<td><input type="text" id="cmTitle" name="cmTitle"></td>
	</tr>
	<tr>
		<th>내용</th>
		<td><textarea id="cmContent" name="cmContent"></textarea></td>
	</tr>
	
</table>
	<button onclick="doInsert()">글쓰기</button> <a href="/views/cm/list"><button>취소</button></a>

<script>
function doInsert(){
	$.ajax({
		url : '/cm4',
		method : 'POST',
		data : {
			
			cmTitle : document.querySelector('#cmTitle').value,
			cmContent : document.querySelector('#cmContent').value
		},
		success : function(res){
			if(res==1){
				alert('글쓰기 성공');
				location.href='/views/cm/list'
			}else{
				alert('글쓰기 실패');
			}
		}
	})
}
</script>
</body>
</html>